# Find next page

## a Firefox extension

This extension allows you to conveniently navigate to the next page when pressing Space at the end of the page

## Installation 

[Find next page](https://addons.mozilla.org/de/firefox/addon/find-next-page/)

## License 

This code is licensed under [Mozilla Public License version 2](https://www.mozilla.org/en-US/MPL/2.0/)
